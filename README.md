# Jest & Puppeteer

Simple tests using Jest test framework and Puppeteer library 

## Getting started


### Prerequisites

* Node v7.6.0 and later
* npm v6 and later


### Install dependencies

```
npm install
```

### Test

```
npm test
```