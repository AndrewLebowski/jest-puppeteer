class Helpers {

    set contextPage( page ) {
        if(!this.page)
        {
            this.page = page;
        }
        else
        {
            return;
        }
    }

    getRandomInt( min, max ) {
        return Math.floor(Math.random() * (max - min) + min);
    };

    clickOnElementNTimes( button, randomNum ) {
        for(let i = 0; i < randomNum; i++) {
            button.click();
        } 
    };

    checkArraysMatching( arr1, arr2 ) {
        let a = arr1.sort().join(', ');
        let b = arr2.sort().join(', ');
        return a === b;
    }; 

    async isLocatorReady(element) 
    {
        const isVisibleHandle = await this.page.evaluateHandle((e) => 
      {
        const style = window.getComputedStyle(e);
        return (style && style.display !== 'none' && 
        style.visibility !== 'hidden' && style.opacity !== '0');
       }, element);
        let visible = await isVisibleHandle.jsonValue();
        const box = await element.boxModel();
        if (visible && box) {
          return true;
        }
        return false;
    };


}

module.exports = new Helpers();