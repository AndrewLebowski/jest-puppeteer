const Page = require('./Page');
const Helpers = require('../Helpers');


class HomePage extends Page {

    set contextPage( page ) {  
        if(!this.page) {
            this.page = page; 
        }
        
    }


    //inputs
    get cityInput() {
        return this.page.$('#ss');
    }
    get childrenAmountInput() {
        return this.page.$('#group_children');
    }
    get adultsInput() {
        return this.page.$('#group_adults');
    }
    get childrenInput() {
        return this.page.$('#group_children');
    }
    get roomsInput() {
        return this.page.$('#no_rooms');
    }
    get peopleInput() { 
        return this.page.$('.xp__guests'); 
    }


    //buttons   
    get addAdultButton() {
        return this.page.$('.sb-group__field-adults .bui-stepper__add-button'); 
    }
    get addChildButton() {
        return this.page.$('.sb-group-children .bui-stepper__add-button'); 
    }  
    get addRoomButton() {
        return this.page.$('.sb-group__field-rooms .bui-stepper__add-button'); 
    }
    get languagesButton() {
        return this.page.$('.uc_language ');
    }
    get englishLanguageButton() {
        return this.page.$('.lang_en-us');
    }
    get addMonthButton() {
        return this.page.$('.bui-calendar__control--next');
    }
    get submitButton() {
        return this.page.$('[type="submit"]');
    }
    get subtractChildButton() {
        return this.page.$('.sb-group-children .bui-stepper__subtract-button');
    }


    //arrays
    get childrenInputsArray() {
        return this.page.$$('.sb-group__children__field select');
    }
    get inputCityListDropdownItems() {
        return this.page.$$('ul[role="listbox"] li');
    }  
    get datesArray() {
        return this.page.$$('.bui-calendar__content td[data-date]');
    }    
    get selectedDatesArray() {
        return this.page.$$('.bui-calendar__date--selected');
    }
    get cityListLarge() {   
        return this.page.$$('.promotion-postcard__large');  
    }
    get cityListSmall() {   
        return this.page.$$('.promotion-postcard__small');
    }


    //checkpints
    get onMapCheckpoint() {
        return this.page.$('[for="sb_results_on_map"]');
    }

   
    async submit() {
        const button = await this.submitButton;
        button.click();
    }

    async selectChildrenAge() 
    {
        await this.page.waitFor(5000);
        const ageInputs = await this.childrenInputsArray;
        for( const input of ageInputs ) 
        {
            await input.click();
            const randomInt = await Helpers.getRandomInt(0, 17);
            await input.type(`${randomInt}`); //type method is used for selecting elements from list dropdown    
        }
    };

    async checkIfAgesAreSelected() 
    { 
        await this.page.waitFor(5000); 
        const resultsArray = [];
        const ageInputs = await this.childrenInputsArray;
        for( const input of ageInputs ) 
        {
            const isNotSelected = await input.$eval('option[selected]', el => el.class );
            if( isNotSelected ) 
            {
                await resultsArray.push(false);
            } 
            else 
            {
                await resultsArray.push(true);
            }
        }
        const agesAreSelected = !resultsArray.includes(false);
        return agesAreSelected; 
    };

    async chooseCity( value ) 
    {
        const input = await this.cityInput;
        await this.page.evaluate( el => el.value = "", input );
        await input.type(value);
        await this.page.waitFor(2000);
        const cityDropdown = await this.inputCityListDropdownItems;
        const maxLength = await cityDropdown.length;
        const randomIndex = await Helpers.getRandomInt( 0, maxLength );
        await cityDropdown[randomIndex].click();
    };

    async chooseRandomDate() 
    {
        //random month
        const randomMonth = await Helpers.getRandomInt(1, 14);
        const addMonth = await this.addMonthButton;
        await Helpers.clickOnElementNTimes( addMonth, randomMonth );
        await this.page.waitFor(8000);
        //random dates
        const randomFirstDate = await Helpers.getRandomInt(1, 14);
        const randomLastDate = await Helpers.getRandomInt(1, 14);
        const dates = await this.datesArray;
        await dates[randomFirstDate].click();
        await dates[randomFirstDate + randomLastDate].click();      
    };

    async selectRandomPeopleAmount() {
        const addAdultsClicks = await Helpers.getRandomInt(1, 10);
        const addChildrenClicks = await Helpers.getRandomInt(1, 10);
        const addRoomClicks = await Helpers.getRandomInt(1, 5);

        const childrenButton = await this.addChildButton;
        const adultButton = await this.addAdultButton;
        const roomButton = await this.addRoomButton;

        await Helpers.clickOnElementNTimes( childrenButton, addChildrenClicks );
        await Helpers.clickOnElementNTimes( adultButton, addAdultsClicks );
        await Helpers.clickOnElementNTimes( roomButton, addRoomClicks  );
    };

    async clearChildrenValue() 
    {   
        const input = await this.childrenAmountInput;
        const value = await input.getProperty('value');
        const jsonValue = await value.jsonValue();  
        const button = await this.subtractChildButton;
        for(let i = 0; i < Number(jsonValue); i++) 
        {
            await button.click()
        }
        
    };

    async getMiliseconds() 
    {    
        let firstDateSeconds;
        let lastDateSeconds;      
        const selectedDates = await this.selectedDatesArray;
        await this.page.waitFor(5000);
        for(let date of selectedDates) 
        {   
            const currentDate = await this.page.evaluate( el => el.getAttribute('data-date'), date )
            const currentDateSeconds = new Date(currentDate).getTime();
            if (!firstDateSeconds) 
            {
                firstDateSeconds = await currentDateSeconds;
            } 
            else 
            {
                lastDateSeconds = await currentDateSeconds;
            }
        }
        return await [firstDateSeconds, lastDateSeconds];
    };

    async getChildAges() 
    {
        let ages = [];
        const array = await this.childrenInputsArray;
        for(let ageInput of array) 
        {
            const selectedAge = await ageInput.$eval('option[selected]', el => el.value);
            await ages.push(selectedAge);
        } 
        return ages;   
    };

    async chooseCityFromList() 
    {
        const cityListLarge = await this.cityListLarge;
        const cityListSmall = await this.cityListSmall;
        const cityList = await cityListLarge.concat(cityListSmall);
        const max = await cityList.length;
        const randomInt = await Helpers.getRandomInt(0, max);
        cityList[randomInt].click();
    };
};

module.exports = new HomePage();




 







// //arrays
// 
// get childrenSelectedInputsArray() {
//     return '.sb-group__children__field option[selected]';
// }
// get datesArray() {
//     return $$('.bui-calendar__content td[data-date]');
// }         
// get selectedDatesArray() {
//     return $$('.bui-calendar__date--selected');
// }
// get inputCityListItemsArray() {
//     return $$('ul[role="listbox"] li');
// }




// //helpers
// get inputCityList() {
//     return $('ul[role="listbox"]');
// }
// get onMapCheckpoint() {
//     return $('[for="sb_results_on_map"]');
// }







