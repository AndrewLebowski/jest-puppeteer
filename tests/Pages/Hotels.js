const Page = require('./Page');
const Helpers = require('../Helpers');


class Hotels extends Page {

    set contextPage( page ) {
        this.page = page;    
    }

    //inputs
    get adultsInput() {
        return this.page.$('#group_adults option[selected]');
    }
    get childrenInput() {
        return this.page.$('#group_children option[selected]');
    }
    get roomsInput() {
        return this.page.$('#no_rooms option[selected]');
    }
    get cityInput() {
        return this.page.$('#ss');
    }
    



    //arrays
    get seeAvailabilityButtonsArray() {
        return this.page.$$('.b-button__text');
    }
    get datesArray() {
        return this.page.$$('[data-offset="262"] td.c2-day');
    }
    get childrenInputsArray() {
        return this.page.$$('.sb-group__children__field select');
    }
    get checkboxesArray() {
        return this.page.$$('[data-id="filter_class"] a');
    }
    get hotelsArray() {
        return this.page.$$('#hotellist_inner .sr_item');
    }
    get reviewScoresArray() {
        return this.page.$$('[data-name="review_score"]');
    }



    get firstDateSeconds() {
        return this.page.$('.c2-day-s-first-in-range'); 
    }
    get lastDateSeconds() {
        return this.page.$('td.c2-day-s-last-in-range'); 
    }
    get calendar() {
        return this.page.$('.sb-date-field__wrapper');
    }
    get hotelsContainer() {
        return this.page.$('#ajaxsrwrap');
    }
    get calendarInput() {
        return this.page.$('.c2-wrapper-s-position-undefined');
    }
    get submitButton() {
        return this.page.$('.sb-searchbox__button');
    }
   
    

    async getChildAges() 
    {
        let ages = [];
        const array = await this.childrenInputsArray;
        for(let ageInput of array) 
        {
            const selectedAge = await ageInput.$('option[selected]');
            let age = await selectedAge.getProperty('value');
            age = await age.jsonValue();
            await ages.push(age);
        } 
        return ages;   
    };
    
    async getDatesSeconds() 
    { 
        const calendar = await this.calendar;
        calendar.click();
        await this.page.waitFor(5000);

        const first = await this.firstDateSeconds;
        let firstResult = await this.page.evaluate( el => el.getAttribute('data-id'), first );
        firstResult = Number(firstResult);

        const second = await this.lastDateSeconds;
        let secondResult = await this.page.evaluate( el => el.getAttribute('data-id'), second );
        secondResult = Number(secondResult);

        return [ firstResult, secondResult ];
    };

    async checkIfCalendarOpenen() 
    {
        const element = await this.calendarInput;
        const elementClass = await this.page.evaluate( el => el.getAttribute('class'), element);
        const result = await !elementClass.includes('c2-wrapper-s-hidden');
        return result;
    };

    async checkHotelStatus() 
    {
        const results = [];
        const hotels = await this.hotelsArray;
        for( let hotel of hotels )
        {     
            const hotelStatus = await hotel.$('.roomrow');
            const soldOut = await hotel.$('.sr_item_content_row');
            const isHotelStatusExist = await hotelStatus !== null;
            const isSoldOutPropertyExist = await soldOut !== null;
            results.push(isHotelStatusExist || isSoldOutPropertyExist);
        }  
        const result = await !results.includes(false) 
        return result
    };

    async clickRandomDetailsButton() 
    {
        const buttons = await this.seeAvailabilityButtonsArray;
        const randomInt = await Helpers.getRandomInt(0, buttons.length);
        buttons[randomInt].click();
    };

    async chooseRandomDate() {   
        const randomDate = await Helpers.getRandomInt(1, 27);
        const arrayOfDates = await this.datesArray;      
        arrayOfDates[randomDate].click();
    };


};

module.exports = new Hotels();













   

 





