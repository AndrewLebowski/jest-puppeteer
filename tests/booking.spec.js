const puppeteer = require('puppeteer');
const HomePage = require('./Pages/HomePage');
const Hotels = require('./Pages/Hotels');
const Helpers = require('./Helpers');




describe("Booking.com, integrate testing", () => {
    let page;
    let browser;
    const url = 'https://booking.com';
    const width = 1000;
    const height = 1400;


    beforeAll(async () => {
        browser = await puppeteer.launch(
        {
            headless: false,
            slowMo: 120,
            args: [`--window-size=${width},${height}`]
        });
        page = await browser.newPage();
        await page.setViewport({ width, height });
        HomePage.contextPage = page;
        Hotels.contextPage = page;
        Helpers.contextPage = page; 
       
    });

    afterAll(() => {
        browser.close();
    });

    afterEach( async () => {
        await page._client.send('Network.clearBrowserCookies');
    });



    test("User is able to specify age of each child", async () => {
        await page.goto( url );
        const randomInteger = await Helpers.getRandomInt( 2, 10 );
        const PeopleInput = await HomePage.peopleInput;
        await PeopleInput.click();


        //add children amount
        const addChild = await HomePage.addChildButton;
        await Helpers.clickOnElementNTimes( addChild, randomInteger );


        //select age for every child
        await HomePage.selectChildrenAge();


        //children amount is equal to random integer
        const children = await HomePage.childrenAmountInput;
        let childrenValue = await children.getProperty('value');
        childrenValue = await childrenValue.jsonValue();
        expect( Number(childrenValue) ).toBe( randomInteger );


        //inputs amount is equal to random integer
        const ageInputs = await HomePage.childrenInputsArray ;
        const ageInputsAnount = await ageInputs.length;
        expect( ageInputsAnount ).toBe( randomInteger );


        //ages are selected
        const areAgesSelected = await HomePage.checkIfAgesAreSelected();
        expect( areAgesSelected ).toBe( true );

        
        //clear children amount value
        await HomePage.clearChildrenValue();


    }, 120000);


    
    test("User is provided with the same search form at search results page", async () => {
        await page.goto( url );


        //choose certain language for further comparison
        const LanguagesButton = await HomePage.languagesButton;
        await page.waitFor(3000);
        await LanguagesButton.click();
        const EnglishButton = await HomePage.englishLanguageButton;
        await EnglishButton.click();
        await page.waitFor(3000);


        //select people amount and children ages
        const PeopleInput = await HomePage.peopleInput;
        await PeopleInput.click();
        await page.waitFor(3000);
        await HomePage.selectRandomPeopleAmount();


        //select age for every child
        await HomePage.selectChildrenAge();


        //write city in the input
        await HomePage.chooseCity('Rivne');


            //get city value for further comparison
            const cityInputHomePage = await HomePage.cityInput;
            let cityValueHomePage = await cityInputHomePage.getProperty('value');
            cityValueHomePage = await cityValueHomePage.jsonValue();
     

        //choose random date
        await HomePage.chooseRandomDate();


            //get dates value for further comparison
            const [HomePageFirstDateSeconds, HomePageLastDateSeconds ] = await HomePage.getMiliseconds();
            

            //get people value for further comparison
            const adultsInputHomePage = await HomePage.adultsInput;
            const childrenInputHomePage = await HomePage.childrenInput;
            const roomsInputHomePage = await HomePage.roomsInput;

            let adultsValueHomePage = await adultsInputHomePage.getProperty('value');
            adultsValue = await adultsValueHomePage.jsonValue();
            let childrenValueHomePage = await childrenInputHomePage.getProperty('value');
            childrenValueHomePage = await childrenValueHomePage.jsonValue();
            let roomsValueHomePage = await roomsInputHomePage.getProperty('value');
            roomsValueHomePage = await roomsValueHomePage.jsonValue();


            //get values of child ages 
            const agesHome = await HomePage.getChildAges();
        

        //prevent map occurring after submit
        const mapCheckpint = await HomePage.onMapCheckpoint;
        const isMapVisible = await Helpers.isLocatorReady(mapCheckpint);      
        if(isMapVisible) 
        {
            await mapCheckpint.click(); 
        }
        

        //submit and wait untill page is loaded
        await HomePage.submit();
        await page.waitFor(10000);


        //correct city
        const cityInputHotels = await Hotels.cityInput;
        let cityValueHotels = await cityInputHotels.getProperty('value');
        cityValueHotels = await cityValueHotels.jsonValue();
        const isCityCorrect = await cityValueHomePage.includes( cityValueHotels );
        expect( isCityCorrect ).toBe( true );


        //correct date
        const [ HotelsFirstDateSeconds, HotelsLastDateSeconds ] = await Hotels.getDatesSeconds();
        const isDatesCorrect = 
            HomePageFirstDateSeconds === HotelsFirstDateSeconds && 
            HomePageLastDateSeconds === HotelsLastDateSeconds;
        expect( isDatesCorrect ).toBe( true );

        
        //correct adult amount 
        const adultsInputHotels = await Hotels.adultsInput;
        let adultsValueHotels = await adultsInputHotels.getProperty('value');
        adultsValueHotels = await adultsValueHotels.jsonValue();
        expect( adultsValueHotels ).toBe( adultsValueHotels );

          
        //correct children amount 
        const childrenInputHotels = await Hotels.childrenInput;
        let childrenValueHotels = await childrenInputHotels.getProperty('value');
        childrenValueHotels = await childrenValueHotels.jsonValue();
        expect( childrenValueHomePage ).toBe( childrenValueHotels );


        //correct rooms amount 
        const roomsInputHotels = await Hotels.roomsInput;
        let roomsValueHotels =  await roomsInputHotels.getProperty('value');
        roomsValueHotels = await roomsValueHotels.jsonValue();
        expect( roomsValueHomePage ).toBe( roomsValueHotels );
        

        //is ages correct
        const agesHotels = await Hotels.getChildAges();  
        const areAgesCorrect = await Helpers.checkArraysMatching( agesHome, agesHotels );  
        expect( areAgesCorrect ).toBe( true );


    }, 120000);



    test('Resulting search entries are taken based on filter', async () => {
        await page.goto( url );


        //choose certain language for further comparison
        const LanguagesButton = await HomePage.languagesButton;
        await page.waitFor(4000);
        await LanguagesButton.click();
        const EnglishButton = await HomePage.englishLanguageButton;
        await EnglishButton.click();
        await page.waitFor(3000);


        //write city in the input
        await HomePage.chooseCity('Rivne');


        //clear children amount value
        const PeopleInput = await HomePage.peopleInput;
        await PeopleInput.click();
        await page.waitFor(3000);     
        await HomePage.clearChildrenValue();


        //prevent map occurring after submit
        const mapCheckpint = await HomePage.onMapCheckpoint;
        const isMapVisible = await Helpers.isLocatorReady(mapCheckpint);      
        if(isMapVisible) 
        {
            await mapCheckpint.click(); 
        }


        //submit and wait untill page is loaded
        await HomePage.submit();
        await page.waitFor(10000);


        //comparison between chosen rating and rating of each hotel on the page
        const checkboxesArray = await Hotels.checkboxesArray;  
        const starsArrayOfResults = [];
        for( let i = 0; i < checkboxesArray.length; i++ )
        {   
            const checkboxes = await Hotels.checkboxesArray;
            const star = await checkboxes[i];
            star.click();
            await page.waitFor(5000);
            const hotelsArray = await Hotels.hotelsArray;
            let hotelClass = await page.evaluate( el =>  el.getAttribute('data-value'), star )
            if( hotelClass === 'Unrated' )
            {
                for( let hotel of hotelsArray )
                {   
                    let unrated = await page.evaluate( el => el.getAttribute('data-class'), hotel)
                    starsArrayOfResults.push( unrated === '0' || unrated === null );
                }
            }
            else
            {
                for( let hotel of hotelsArray)
                {   
                    let stars = await page.evaluate( el => el.getAttribute('data-class'), hotel)
                    if( stars === '0')
                    {
                        let bookingStars = await hotel.$$('span svg.-iconset-square_rating');
                        bookingStars = bookingStars.length;
                        stars = bookingStars.toString();
                    }
                    starsArrayOfResults.push( hotelClass === stars || hotelClass === null);
                }
            }  
            const Checkboxes = await Hotels.checkboxesArray; 
            const Star = await Checkboxes[i];
            Star.click(); 
            await page.waitFor(3000);

        }
        const isStarsAmountCorrect = await !starsArrayOfResults.includes( false );  
        expect( isStarsAmountCorrect ).toBe( true );



        //each hotel has scored review corresponding to chosen
        const reviewScoresArray = await Hotels.reviewScoresArray; 
        const scoresArrayOfResults = [];
        for( let i = 0; i < reviewScoresArray.length; i++ )
        {
            const reviewScores = await Hotels.reviewScoresArray;
            const score = await reviewScores[i];
            await score.click();
            await page.waitFor(5000);
            let scoreValue = await page.evaluate( el =>  el.getAttribute('data-value'), score );
            scoreValue = Number(scoreValue) / 10;
            const hotelsArray = await Hotels.hotelsArray;
            if( scoreValue === 99.9 )
            {
                for( let hotel of hotelsArray )
                {
                    const unscored = await page.evaluate( el => el.getAttribute('data-score'), hotel );
                    scoresArrayOfResults.push( unscored !== Number );
                }
            }
            else
            {
                for( hotel of hotelsArray )
                {
                    let hotelScore = await page.evaluate ( el => el.getAttribute('data-score'), hotel );
                    hotelScore = Number(hotelScore);
                    scoresArrayOfResults.push( hotelScore >= scoreValue );
                }
            }
            const ReviewScores = await Hotels.reviewScoresArray;
            const Score = await ReviewScores[i];
            await Score.click();
            await page.waitFor(3000);
        }
        const isScoreViewsCorrect = await !scoresArrayOfResults.includes(false); 
        expect( isScoreViewsCorrect ).toBe( true );


    }, 180000);


    
    test('User is required to specify booking date to see booking price', async () => {
        await page.goto( url );
        await HomePage.chooseCityFromList();
        await page.waitFor(7000);


        //is page with hotels opened
        const hotelsContainer = await Hotels.hotelsContainer;
        const areHotelsOpen = await Helpers.isLocatorReady(hotelsContainer);
        expect( areHotelsOpen ).toBe( true );


        // is calendar opened before click on Details button
        const isCalendarOpened = await Hotels.checkIfCalendarOpenen();
        expect( isCalendarOpened ).toBe( true );


        //are details visible before click on Details button
        const isHotelsStatusVisible = await Hotels.checkHotelStatus();
        expect( isHotelsStatusVisible ).toBe( false );

        await page.waitFor(3000);
        await Hotels.clickRandomDetailsButton();
        await page.waitFor(3000);


        //is calendar opened after click on Details button
        const IsCalendarOpened = await Hotels.checkIfCalendarOpenen();
        expect( IsCalendarOpened ).toBe( true );

        await Hotels.chooseRandomDate();
        const submit = await Hotels.submitButton;
        await submit.click();
        await page.waitFor(3000);


        //are details visible after submit
        const IsHotelsStatusVisible = await Hotels.checkHotelStatus();
        expect( IsHotelsStatusVisible ).toBe( true );

        
    }, 120000);
});




